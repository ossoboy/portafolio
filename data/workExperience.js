export default [
  {
    title: "Ludo Gangs Landing Page",
    description:
      'Desarrollo e implementación de una Landing Page para el juego "Ludo Gangs" presentada en diferentes idiomas',
    date: "Junio 2021",
    stack: ["sass", "bootstrap", "nuxt"],
    company: 'KOA STUDIOS / Front-end developer',
    gallery: [
      {
        image: "/images/projects/ludogangs/hero.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/ludogangs/game.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/ludogangs/gangs.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/ludogangs/warrior-description.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/ludogangs/footer.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/ludogangs/team.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/ludogangs/hero-vertical.png",
        title: "",
        desc: "",
        portrait: true
      },
      {
        image: "/images/projects/ludogangs/gangs-vertical.png",
        title: "",
        desc: "",
        portrait: true
      },
      {
        image: "/images/projects/ludogangs/warrior-description-vertical.png",
        title: "",
        desc: "",
        portrait: true
      },
      {
        image: "/images/projects/ludogangs/team-vertical.png",
        title: "",
        desc: "",
        portrait: true
      },
    ],
  },
  {
    title: "biosesquemascolegas.com",
    description:
      "Aplicación Web Privada que funciona como puente para descargar archivos de una página externa protegida, Aplicando Web Scraping, Cookies y Sesiones",
    date: "Mayo 2021",
    stack: ["laravel", "bootstrap", "mariadb"],
    company: 'Full Stack developer',
    gallery: [
      {
        image: "/images/projects/biosesquemascolegas/download.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/biosesquemascolegas/credentials.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/biosesquemascolegas/users.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/biosesquemascolegas/log.png",
        title: "",
        desc: "",
      },
    ],

  },
  {
    title: "Bolsa de Trabajo - Instituto Urusayhua",
    description:
      "Desarrollo e implementación de un Portal de Bolsa de Trabajo y Panel de Administración para el instituto Urusayhua (Cusco, Perú).",
    date: "Abril 2021",
    stack: ["sass", "bootstrap", "laravel", "vue", "mariadb"],
    company: 'Full Stack developer',
    gallery: [
      {
        image: "/images/projects/bolsatrabajo.urusayhua/home.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/bolsatrabajo.urusayhua/company.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/bolsatrabajo.urusayhua/job.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/bolsatrabajo.urusayhua/dialog.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/bolsatrabajo.urusayhua/jobs.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/bolsatrabajo.urusayhua/jobs-form.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/bolsatrabajo.urusayhua/company-form.png",
        title: "",
        desc: "",
      },
    ],

  },
  {
    title: "Raquelyn Rozas Landing Page",
    description: "Desarrollo e implementación de una Landing Page.",
    date: "Marzo 2021",
    stack: ["sass", "bootstrap", "nuxt"],
    company: 'INKADEVS / Front-end developer',
    gallery: [
      {
        image: "/images/projects/raquelynrozas/hero.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/raquelynrozas/content.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/raquelynrozas/videos.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/raquelynrozas/collapse.png",
        title: "",
        desc: "",
      },
    ],

  },
  {
    title: "Americana de Turismo",
    description: `Diseño, desarrollo e implementación de una Aplicación Web a medida para la empresa operador de turístico "Americana de Turismo". Aplicando uso de APIs, JWT tokens y Roles de Usuario.`,
    gallery: false,
    date: "2020 - 2021",
    stack: ["sass", "bootstrap", "laravel", "vue", "jwt", "mariadb"],
    company: 'INKADEVS / Full Stack developer',
  },
  {
    title: "Letras Bien Escritas",
    description: `Desarrollo e implementación de un Sitio Web a medida, donde encontrarás las letras "bien escritas" en un catálogo musical agrupados por género, década, país, idioma y artistas.`,
    date: "2020",
    stack: ["sass", "bootstrap", "laravel", "react", "next", "mariadb"],
    company: 'INKADEVS / Full Stack developer',
    gallery: [
      {
        image: "/images/projects/letrasbienescritas/home.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/letrasbienescritas/footer.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/letrasbienescritas/artists.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/letrasbienescritas/artist.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/letrasbienescritas/songs.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/letrasbienescritas/song.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/letrasbienescritas/lyrics.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/letrasbienescritas/musical-genres.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/letrasbienescritas/musical-genre.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/letrasbienescritas/countries.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/letrasbienescritas/country.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/letrasbienescritas/idioms.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/letrasbienescritas/idiom.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/letrasbienescritas/vocalists.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/letrasbienescritas/vocalist.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/letrasbienescritas/search-results.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/letrasbienescritas/home-portrait.png",
        title: "",
        desc: "",
        portrait: true
      },
      {
        image: "/images/projects/letrasbienescritas/footer-portrait.png",
        title: "",
        desc: "",
        portrait: true
      },
      {
        image: "/images/projects/letrasbienescritas/menu-portrait.png",
        title: "",
        desc: "",
        portrait: true
      },
    ],

  },
  {
    title: "API REST Disponibilidad de Boletos - Andean Adventures Perú",
    description: `Desarrollo de una API REST que retorna la disponibilidad de boletos por ruta para su reserva e ingreso a la maravilla Machu Picchu (Cusco, Perú)`,
    gallery: false,
    date: "2020",
    stack: ["nodejs", "expressjs"],
    company: 'INKADEVS / Back-end developer',
  },
  {
    title: "SIARA - Municipalidad Distrital de Echarati",
    description: `Desarrollo e implementación de una aplicación web para el control y seguimiento de proyectos de inversión pública, sincronizado con el Ministerio de Economía y Finanzas (MEF), Sistema de Seguimiento de Inversiones (SSI) y SIADEG, aplicando uso de Web Scraping, APIs, JWT tokens y Roles de Usuario`,
    date: "2019",
    stack: ["sass", "bootstrap", "laravel", "vue", "jwt", "mariadb"],
    company: 'Full Stack developer',
    gallery: [
      {
        image: "/images/projects/siara/landing.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/landing-benefits.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/landing-how-to.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/landing-contact.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/home.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/columns.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/semaphore.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/resume-1.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/resume-2.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/report-1.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/report-2.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/synchronization.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/configuration.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/users.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/work.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/work-data.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/work-advance.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/work-history.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/work-ubication.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/work-print.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/siara/work-siadeg.png",
        title: "",
        desc: "",
      }
    ],
    
  },
  {
    title: "TuGuía",
    description: `Desarrollo e implementación de una aplicación web, una guía virtual gratuita basada en las preferencias del usuario.`,
    date: "2019",
    stack: ["sass", "bootstrap", "laravel", "vue", "mariadb"],
    company: 'INKADEVS / Full Stack developer',
    gallery: [
      {
        image: "/images/projects/tuguia/home.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/tuguia/home-articles.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/tuguia/blog.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/tuguia/article.png",
        title: "",
        desc: "",
      },

      {
        image: "/images/projects/tuguia/categories.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/tuguia/category.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/tuguia/company-home.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/tuguia/company-info-1.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/tuguia/company-info-2.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/tuguia/company-maps.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/tuguia/gallery.png",
        title: "",
        desc: "",
      },

      {
        image: "/images/projects/tuguia/offers.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/tuguia/results.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/tuguia/results-grid.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/tuguia/results-list.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/tuguia/user.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/tuguia/user-categories.png",
        title: "",
        desc: "",
      },
    ],
    
  },
  {
    title: "INKADEVS Landing Page",
    description: `Desarrollo e implementación de una Landing Page para la empresa INKA DEVS.`,
    date: "2019",
    stack: ["sass", "bootstrap", "nuxt"],
    company: 'INKADEVS / Front-end developer',
    gallery: [
      {
        image: "/images/projects/inkadevs/home.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/inkadevs/about-us.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/inkadevs/projects.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/inkadevs/services.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/inkadevs/footer.png",
        title: "",
        desc: "",
      },
    ],
  },
  {
    title: "Dashboard Universidad Privada Líder Peruana",
    description: `Desarrollo e implementación de una aplicación web, un panel de administración para las publicaciones en la página principal, control de papeletas de salida, firma y directorio.`,
    date: "2017",
    stack: ["sass", "bootstrap", "laravel", "vue", "mariadb"],
    gallery: [
      {
        image: "/images/projects/ulp.dashboard/home.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/ulp.dashboard/users.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/ulp.dashboard/user.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/ulp.dashboard/directory.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/ulp.dashboard/exit-permits.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/ulp.dashboard/firm.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/ulp.dashboard/ulp-carrusel.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/ulp.dashboard/report.png",
        title: "",
        desc: "",
      },
    ],
    
  },
  {
    title: "Grupo Tutupaca",
    description: `Desarrollo e implementación de un módulo de noticias`,
    date: "2017",
    stack: ["bootstrap", "codeigniter"],
    gallery: [
      {
        image: "/images/projects/grupotutupaca/home.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/grupotutupaca/news.png",
        title: "",
        desc: "",
      },
      {
        image: "/images/projects/grupotutupaca/news-1.png",
        title: "",
        desc: "",
      },
    ],
    
  },
]