export default {
  technologies: [
    {
      image: "javascript.png",
      name: "JavaScript",
    },
    {
      image: "php.png",
      name: "php",
    },
    {
      image: "bootstrap.png",
      name: "Bootstrap",
    },
    {
      image: "sass.png",
      name: "Sass",
    },
    {
      image: "laravel.png",
      name: "Laravel",
    },
    {
      image: "vue.png",
      name: "Vue",
    },
    {
      image: "nuxt.png",
      name: "Nuxt",
    },
    {
      image: "react.png",
      name: "React",
    },
    {
      image: "next.png",
      name: "Next",
    },
    {
      image: "nodejs.png",
      name: "Node.js",
    },
    {
      image: "expressjs.png",
      name: "Express.js",
    },
    {
      image: "c-sharp.png",
      name: "C#",
    },
    {
      image: "mariadb.png",
      name: "Maria DB",
    },
    {
      image: "jwt.png",
      name: "JWT",
    },
    {
      image: "git.png",
      name: "Git",
    },
  ],
  learning: [
    {
      image: "angular.png",
      name: "Angular",
    },
    {
      image: "flutter.png",
      name: "Flutter",
    },
    {
      image: "figma.png",
      name: "Figma",
    },
    {
      image: "unity.png",
      name: "Unity",
    },
  ],
}